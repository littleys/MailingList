/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mailinglist.controller;

import com.mycompany.mailinglist.controller.exceptions.NonexistentEntityException;
import com.mycompany.mailinglist.controller.exceptions.RollbackFailureException;
import com.mycompany.mailinglist.entities.Account;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;

/**
 *
 * @author Yushu
 */
@Named("accountJpaController")
@RequestScoped
public class AccountJpaController implements Serializable {

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;
    
    public void create(Account account) throws RollbackFailureException, Exception {
        try {
            utx.begin();
            em.persist(account);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } 
    }

    public void edit(Account account) throws NonexistentEntityException, RollbackFailureException, Exception {
       try {
            utx.begin();
            account = em.merge(account);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = account.getId();
                if (findAccount(id) == null) {
                    throw new NonexistentEntityException("The account with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
       
        try {
            utx.begin();
            Account account;
            try {
                account = em.getReference(Account.class, id);
                account.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The account with id " + id + " no longer exists.", enfe);
            }
            em.remove(account);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } 
    }

    public List<Account> findAccountEntities() {
        return findAccountEntities(true, -1, -1);
    }

    public List<Account> findAccountEntities(int maxResults, int firstResult) {
        return findAccountEntities(false, maxResults, firstResult);
    }

    private List<Account> findAccountEntities(boolean all, int maxResults, int firstResult) {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Account.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } 


    public Account findAccount(Integer id) {

            return em.find(Account.class, id);

    }

    public Account findAccount(String email, String password) {
        TypedQuery<Account> query = em.createNamedQuery("Account.findByEmailAndPassword", Account.class);
        query.setParameter(1, email);
        query.setParameter(2, password);
        List<Account> accounts = query.getResultList();
        if (!accounts.isEmpty()) {
            return accounts.get(0);
        }
        return null;
    }

    /**
     * Find a record with the specified email
     *
     * @param email
     * @return
     */
    public Account findAccountByEmail(String email) {
        TypedQuery<Account> query = em.createNamedQuery("Account.findByEmail",Account.class);
        query.setParameter("email", email);
        List<Account> accounts = query.getResultList();
        if (!accounts.isEmpty()) {
            return accounts.get(0);
        }
        return null;
    }
    
    
    
    
    public int getAccountCount() {

            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Account> rt = cq.from(Account.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
 
    }
    
}
