/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mailinglist.backing;

import com.mycompany.mailinglist.beans.Download;
import com.mycompany.mailinglist.controller.AccountJpaController;
import com.mycompany.mailinglist.entities.Account;
import com.mycompany.util.MessagesUtil;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Yushu
 */
@Named("accountBacking")
@RequestScoped
public class AccountBackingBean implements Serializable {

    @Inject
    private AccountJpaController accountJpaController;
    private Account account;
    private String email;
    private String password;
    private static final Logger LOG = Logger.getLogger("ClientJpaController.class");
    private Download download;

    public Account getAccount() {
        if (account == null) {
            account = new Account();
        }
        return account;
    }

    public String createAccount() throws Exception {
        if (isValidEmail()) {
            accountJpaController.create(account);
            return "signin";
        }
        return null;
    }

    public String getEmail() {
        LOG.info(email);
        return email;
    }

    public void setEmail(String email) {
        LOG.info(email);
        this.email = email;
    }

    public String getPassword() {
        LOG.info(password);
        return password;
    }

    public void setPassword(String password) {
        LOG.info(password);
        this.password = password;

    }

    private boolean isValidEmail() {
        boolean valid = false;
        String email = account.getEmail();
        if (email != null) {
            if (accountJpaController.findAccountByEmail(email) == null) {
                valid = true;
            } else {
                FacesMessage message = MessagesUtil.getMessage(
                        "com.mycompany.bundles.messages", "email.in.use", null);
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage("signupForm:email", message);
            }
        }
        return valid;
    }

    public String login() {

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        LOG.log(Level.INFO, "Entering login method email is {0} password is {1}", new Object[]{email, password});
        FacesMessage message;
        boolean loggedIn = false;
        Account account = accountJpaController.findAccount(email, password);
        if (account != null) {
            loggedIn = true;
            
            //download.setLoggedIn(true);
            return "download";
        } else {
            download.setLoggedIn(true);
            
            loggedIn = false;
            message = MessagesUtil.getMessage(
                    "com.mycompany.bundles.messages", "loginerror", null);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage("signupForm:password", message);
            return "frontPage";
        }
//        // Store the outcome in the session object
//        session.setAttribute("loggedIn", loggedIn);
//
//        // Place the message in the context so that it will be displayed
//        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
