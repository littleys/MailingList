-- Best practice MySQL as of 5.7.6

-- This script needs to run only once

DROP DATABASE IF EXISTS CLIENT;
CREATE DATABASE CLIENT;

USE CLIENT;

DROP USER IF EXISTS account@localhost;
CREATE USER account@'localhost' IDENTIFIED WITH mysql_native_password BY 'concordia' REQUIRE NONE;
GRANT ALL ON CLIENT.* TO account@'localhost';

FLUSH PRIVILEGES;
