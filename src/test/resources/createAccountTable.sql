-- Best practice MySQL as of 5.7.6
--


USE CLIENT;

DROP TABLE IF EXISTS ACCOUNT;

CREATE TABLE ACCOUNT (
  ID int(10) NOT NULL auto_increment,
  EMAIL varchar(50) NOT NULL default '',
  PASSWORD varchar(20) NOT NULL default '',
  FIRSTNAME varchar(20) NOT NULL default '',
  LASTNAME varchar(20) NOT NULL default '',
  ADDRESS varchar(100) NOT NULL default '',
  CITY varchar(30) NOT NULL default '',
  PROVINCE varchar(30) NOT NULL default '',
  COUNTRY varchar(30) NOT NULL default '',
  ZIP varchar(10) NOT NULL default '',
  PRIMARY KEY  (ID)
) ENGINE=InnoDB;

INSERT INTO ACCOUNT (EMAIL,PASSWORD,FIRSTNAME,LASTNAME,ADDRESS,CITY,PROVINCE,COUNTRY,ZIP)values
("alex@gamil.com","123456","Alex","Ray","985 Rue Nortre-Dame Ouest","Montreal","QC","Canada","H5Q1S4"),
("ben@gamil.com","123456","Ben","King","877 Rue St-Cathrine Ouest","Quebec","QC","Canada","J7E0A0"),
("zoe@gamil.com","123456","Zoe","Lee","26 First Street","Ottawa","ON","Canada","A6Y1H5");
